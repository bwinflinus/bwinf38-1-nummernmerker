// The main file for solving the task

const OUT = document.getElementById("out");

const NUMBER = prompt("Welche Nummer soll zum Merken aufbereitet werden?", "005480000005179734");
OUT.innerHTML = runNumber(NUMBER);

export function runNumber(number) {
	console.time("Laufzeit");
	let blocks = R4(number);
	if (!isSuboptimal(blocks)) {
		// passt so
	} else {
		// Spalten
		blocks = splitAfterChunksOfZeros([number]);
		blocks = splitBeforeDigitsBeforeChunksOfZeros(blocks);
		blocks = blocks.map(b => R4(b)).reduce((arr, bs) => [...arr, ...bs], []);
		blocks = connectSingleDigitBlocks(blocks);
	}
	console.timeEnd("Laufzeit");
	let res = blocks.join(" ");
	console.log(res);
	return res;
}

/** @typedef {[string]} Blocks Ein Array von Blöcken mit Ziffern */

/**
 * 4er-Regel anwenden
 * @param {string} number
 * @returns {Blocks}
 */
function R4(number) {
	// Sonderfall für wenn number nur eine Ziffer hat
	if (number.length === 1) return [number];

	let blocks = [];
	let pos = 0;
	if (number.length % 4 !== 1) {
		// 4er-Blöcke und als letztes 2, 3 oder 4 Ziffern
		while (pos < number.length) {
			blocks.push(number.substr(pos, 4));
			pos += 4;
		}
	} else {
		// 4er-Blöcke und am Ende ein 3er- und ein 2er-Block
		while (pos < number.length-5) {
			blocks.push(number.substr(pos, 4));
			pos += 4;
		}
		blocks.push(number.substr(pos, 3), number.substr(pos+3, 2));
	}
	return blocks;
}

function isSuboptimal(blocks) {
	// Blöcke sind suboptimal, wenn einer der Blöcke außer der erste mit 0 beginnt
	return blocks.join(" ").includes(" 0");
}

/**
 * Nach allen Null-Ketten spalten
 * @param {Blocks} blocks
 * @returns {Blocks}
 */
function splitAfterChunksOfZeros(blocks) {
	let newBlocks = [];
	for (const b of blocks) {
		let regex = /0+/g;
		let lastEnd = 0;
		while (regex.exec(b) != null) {
			newBlocks.push(b.slice(lastEnd, regex.lastIndex));
			lastEnd = regex.lastIndex;
		}
		newBlocks.push(b.substr(lastEnd));
	}
	return newBlocks;
}

/**
 * Nach allen Null-Ketten spalten
 * @param {Blocks} blocks
 * @returns {Blocks}
 */
function splitBeforeDigitsBeforeChunksOfZeros(blocks) {
	let newBlocks = [];
	for (const b of blocks) {
		let regex = /\d0+/g;
		let lastEnd = 0;
		let match;
		while ((match = regex.exec(b)) != null) {
			if (match.index === 0) break;
			newBlocks.push(b.slice(lastEnd, match.index));
			lastEnd = match.index;
		}
		newBlocks.push(b.substr(lastEnd));
	}
	return newBlocks;
}
/**
 * Alle Blöcke mit nur einer Ziffer werden mit anderen verbunden
 * @param {Blocks} blocks
 * @returns {Blocks}
 */
function connectSingleDigitBlocks(blocks) {
	blocks.forEach((current, i) => {
		if (current.length === 1) {
			// Block mit nur einer Ziffer entdeckt
			// Nach kürzestem Nachbar-Block schauen
			if (!blocks[i-1]) {
				// Es gibt keinen linken Nachbar -> zum rechten Nachbar verbinden
				blocks[i] += blocks[i+1];
				delete blocks[i+1];
				if (blocks[i].length > 4) {
					// Der rechte Nachbar war 4 Ziffern lang
					// -> der jetzt entstandene Block ist 5 Ziffern lang
					// -> 4er-Regel anwenden
					let newBlocks = R4(blocks[i]);
					blocks[i] = newBlocks[0];
					blocks[i+1] = newBlocks[1];
				}
			} else if (!blocks[i+1]) {
				// Es gibt keinen rechten Nachbar -> zum linken Nachbar verbinden und ggf. 4er-Regel anwenden
				blocks[i-1] += current;
				delete blocks[i];
				if (blocks[i-1].length > 4) {
					let newBlocks = R4(blocks[i-1]);
					blocks[i-1] = newBlocks[0];
					blocks[i] = newBlocks[1];
				}
			} else if (blocks[i-1].length < 4 && blocks[i-1].length <= blocks[i+1].length) {
				//                             /\ Nach Regel 5 bevorzugt nach links verbinden
				// -> Auch wenn beide Nachbar-Blöcke kürzer als 4 Ziffern und gleich lang sind, dann wird nach links verbunden
				// Es soll zum linken Nachbar verbunden werden
				blocks[i-1] += current;
				delete blocks[i];
			} else if (blocks[i+1].length < 4 && blocks[i+1].length < blocks[i-1].length) {
				// Es soll zum rechten Nachbar verbunden werden
				blocks[i] += blocks[i+1];
				delete blocks[i+1];
			} else {
				// Beide Nachbar-Blöcke sind 4 Ziffern lang
				// * -> Nach links verbinden und und den nun 5 Ziffern langen Block nach der zweiten Ziffer spalten
				// * effektiv das gleiche wie die letzten beiden Ziffern des vorherigen Blocks zu nehmen und an den aktuellen Block anzuschließen
				blocks[i-1] += current;
				blocks[i] = blocks[i-1].substr(2, 3);
				blocks[i-1] = blocks[i-1].substr(0, 2);
			}
		}
	});
	// alle leeren Array-Einträge die eben entstanden sein könnten entfernen
	blocks = blocks.filter(b => b);
	return blocks;
}
