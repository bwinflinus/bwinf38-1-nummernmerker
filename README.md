# bwinf38-1-Nummernmerker

Bearbeitung der Aufgabe2: Nummernmerker der ersten Runde des 38. BWINF 2019/20
https://bwinf.de/bundeswettbewerb/38/1-runde/

## Vorgehensweisen

1. 4er-Regel:
Alles in Vierer-Blöcke aufteilen  
Wenn der letzte Block nur 1 lang ist, noch eine Ziffer vom Vorblock nehmen

2. Immer vor der Zahl trennen, die vor einer 0 ist.  
Dadurch werden (in den meisten Fällen?) möglichst wenig Blöcke mit 0 begonnen.

3. Immer nach 0er-Ketten spalten

4. Einzelne Ziffern zum kürzesten Nachbarn hinzufügen

5. Einzelne Ziffern nach links verbinden

## Algorithmus

- 4er-Regel anwenden
- Wenn dies zu einem offenbar suboptimalen Ergebnis führt (Blöcke starten mit 0, Erster Block ausgenommen),
  - dann gehe folgendermaßen auf Grundlage des Originals vor:
  - Nach Regel 2 und 3 spalten
  - Nun beginnen alle Blöcke mit einer Ziffer!=0
  - 4er-Regel anwenden (mit oder ohne Rest-Berücksichtigung weiß ich noch nicht)
  - Einzelne Ziffern (mit RB nur Ziffern!=0) nach Regel 4 an den kürzesten Nachbarblock anschließen
  - Wenn beide Nachbarblöcke bereits 4 Ziffern haben
    - dann nach Regel 5 nach links anschließen und 4er-Regel anwenden

## Von Hand gelöst:

005480000005179734  
0054 8000 0005 1797 34
Ist zwar optimal, wird aber vom programm als suboptimal erkannt

03495929533790154412660  
0349 5929 5337 9015 4412 660

5319974879022725607620179  
5319 9748 7902 2725 6076 2017 9 !  
5319 9748 7902 2725 6076 201 79

9088761051699482789038331267  
9088 7610 5169 9482 7890 3833 1267

> 1 kann im Folgenden für alle Ziffern außer 0 stehen

*Nach alten Regeln:*  
011000000011000100111111101011  
0110 0000 0011 0001 0011 1111 1010 11 !  
01 100000001 1000 100111111 10 10 11 !  
01 1000 000 01 1000 1001 111 11 10 10 11  
01 1000 000 **01** 1000 1001 111 11 10 10 11  
Es geht nicht weniger, weil wenn man den `01` Block aufteilt wird dafür ein anderer Block mit 0 begonnen:  
01 1000 000**0 1100 0**100 1111 11 10 10 11 oder  
01 1000 000**0 110 00** 1001 111 11 10 10 11

*Experimentell:*  
Aber mit einer Abänderung:  
01100000001100**0->1**100111111101011  
01 100000001 1001 100111111 10 1011  
01 1000 000 **01** 1001 1001 111 11 10 1011  
Hier geht auf diese Weise weniger:  
01 1000 000**0 1100 1100 1**111 11 10 1011

Zusätzlich immer nach 0er-Blöcken spalten:  
01100000001100100111111101011  
0 1 10000000 1 100 1 100 111111 10 10 11  
Dann 4er-Regel:  
0 1 1000 0000 1 100 1 100 1111 11 10 10 11  
Und dann einzelne Ziffern an den ?kürzesten Nachbarblock? anschließen:  
01 1000 0000 1100 1100 1111 11 10 10 11  
Führt in diesem Fall zur optimalen Lösung  

Noch eine Änderung:
0110000000**0**1100100111111101011
0110 0000 0001 1001 0011 1111 1010 11  !suboptimal  
0 1 100000000 1 100 100 111111 10 10 11  
4er-Regel ohne Rest-berücksichtigung  
0 1 1000 0000 0 1 100 100 1111 11 10 10 11  
Verbinden an den kürzesten Nachbarblock  
01 1000 0000 01 100 100 1111 11 10 10 11  
ist optimal

Noch eine Änderung:  
011000000**00->nix**1100100111111101011
011 0000 0011 0010 0111 1111 010 11  !suboptimal  
0 1 1000000 1 100 100 111111 10 10 11  
0 1 1000 **000 1 100** 100 1111 11 10 10 11  
Verbinden bei gleichlangen Nachbarn nach links  
01 1000 **0001 100** 100 1111 11 10 10 11  
Weil es hätte ja auch eine 0 alleine sein können, wenn die nach rechts rutscht produziert sie garantiert einen neuen Block, der mit 0 beginnt

Noch eine Änderung:
01 1000 **0000 1 1000** 100 1111 11 10 10 11
Nach Links und dann solange 4er-Regel und Verbinden im Wechsel bis keine 5er oder 1er Gruppe mehr besteht:
01 1000 00001 1000 100 1111 11 10 10 11
01 1000 000 01 1000 100 1111 11 10 10 11
Oder nach Links verbinden und einen abtrennen: klappt aber nicht immer (nur noch 4er-Blöcke links daneben)
01 1000 00001 1000 100 1111 11 10 10 11
01 1000 0 0001 1000 100 1111 11 10 10 11
01 10000 0001 1000 100 1111 11 10 10 11
01 1 0000 0001 1000 100 1111 11 10 10 11
011 0000 0001 1000 100 1111 11 10 10 11
Außerdem ist das ne ziemlich beschissene Lösung

*An der Stelle implementiere ich mal das, was ich bisher habe*